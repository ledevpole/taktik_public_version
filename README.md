# Salut!

Comment installer ce logiciel?
Déjà, avoir python sur sa machine, ça aide..
[ici, par exemple](https://www.python.org/downloads/)


Comment télécharger le logiciel via git?

    git clone https://gitlab.com/ledevpole/taktik_public_version.git

Si vous avez déjà pip (fourni souvent de base avec python),
merci d'installer les dépendances via:

    pip install -r requirements.txt

Vous pouvez lancer le logiciel en ligne de commande via:

    flask run
