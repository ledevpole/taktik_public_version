window.addEventListener('load', (event) => {

    // Mailing form dynamisation
    let radio1 = document.getElementById("body_text")
    let radio2 = document.getElementById("body_file")

    radio1.addEventListener("click",showBodyText);
    radio2.addEventListener("click",showBodyFile);

    showBodyFile();

    function showBodyText() {
        document.getElementById("body_text_div").hidden = false;
        document.getElementById("body_file_div").hidden = true;
    }

    function showBodyFile() {
        document.getElementById("body_text_div").hidden = true;
        document.getElementById("body_file_div").hidden = false;
    }

    // Mailing logic for drag'n'drop
    window.start = function start(e) {
        e.dataTransfer.effectAllowed="move";
        e.dataTransfer.setData("text",e.target.getAttribute("id"));
    }

    window.over = function over(e) {
        e.currentTarget.className="drop_over";
       e.preventDefault();
    }

    window.overback = function overback(e) {
        e.currentTarget.className="text_source_over";
       e.preventDefault();
    }
    window.drop = function drop(e) {
        ob=e.dataTransfer.getData("text");
        e.currentTarget.appendChild(document.getElementById(ob));
        e.currentTarget.className="droppable";
        parseDropAreaToHiddenField();
        e.stopPropagation();
    }

    window.dropback = function dropback(e) {
        ob=e.dataTransfer.getData("text");
        e.currentTarget.appendChild(document.getElementById(ob));
        e.currentTarget.className="text_source";
        parseDropAreaToHiddenField();
        e.stopPropagation();
    }

    window.leave = function leave(e) {
        e.currentTarget.className="droppable";
    }

    window.leaveback = function leaveback(e) {
        e.currentTarget.className="text_source";
    }


    // Create draggables elements on fly and add it to hidden form input

    //get showed textarea value on add button click

    let add_txt_btn = document.getElementById("add_txt_btn")

    add_txt_btn.addEventListener("click", addTxtToSourceDiv);

    //add it in the text_source div as a draggable paragraph
    function addTxtToSourceDiv() {
        let txt = document.getElementById("add_txt").value;
        if(txt != "") {
            const para = document.createElement("p");
            const node = document.createTextNode(txt);
            para.appendChild(node);
            para.setAttribute("draggable", "true");
            para.setAttribute("id", Math.floor(Math.random() * 10000000) + 1);
            const element = document.getElementById("text_source");
            element.appendChild(para);
            document.getElementById("add_txt").value = "";
        }
    }

    //on onDragLeave i guess, update hidden textarea value by getting all
    //paragraphs in the drop area..

    function parseDropAreaToHiddenField() {
        let div = document.getElementById("droppable");
        let children = [...div.children];
        document.getElementById("body").textContent = "";

        var content = "";
        children.forEach(c => content += c.textContent + "<br/>");

        console.log(content);
        document.getElementById("body").textContent = content;
    }
});

