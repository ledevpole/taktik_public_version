from reportlab.platypus import SimpleDocTemplate, Paragraph, Spacer, Frame
from reportlab.lib.styles import getSampleStyleSheet
from reportlab.rl_config import defaultPageSize
from reportlab.lib.units import inch, cm
from reportlab.lib import colors
from reportlab.lib.utils import ImageReader
from reportlab.pdfgen import canvas
import json

PAGE_HEIGHT=defaultPageSize[1]
PAGE_WIDTH=defaultPageSize[0]
styles = getSampleStyleSheet()

styleN = styles["Normal"]
styleH1 = styles['Heading1']
styleH2 = styles['Heading2']

def article(mode,Story, title = "", content = []):
    main_sp = 0.2
    c_sp = 0.25

    if(mode == 'light'):
        main_sp = 0.3
        c_sp = 0.35

    if(mode == 'dense'):
        main_sp = 0.1
        c_sp = 0.15

    p = Paragraph(title,styleH2)
    Story.append(p)
    Story.append(Spacer(1,main_sp*cm))

    for c in content:
        p = Paragraph(c,styleN)
        Story.append(p)
        Story.append(Spacer(1,c_sp*cm))

    return Story

def rgb(r,g,b):
    return (r/256,g/256,b/256)

def create_basic_resume(data,img,name,color,mode,reverse = False):
    doc = canvas.Canvas(name)
    Story = []
    f = Frame(6*cm,24*cm,PAGE_WIDTH-(7*cm),5*cm)
    r,g,b = color
    doc.setFillColorRGB(r,g,b)
    doc.setStrokeColorRGB(r,g,b)
    if reverse == False:
        doc.rect(0,0,(PAGE_WIDTH /2),PAGE_HEIGHT,stroke=1,fill=1)
    else:
        doc.rect((PAGE_WIDTH /2),0,(PAGE_WIDTH /2),PAGE_HEIGHT,stroke=1,fill=1)
    doc.drawImage(img, 2*cm, 24*cm,100,130)

    p = Paragraph(data["header"]["label"],styleH1)
    Story.append(p)

    p = Paragraph(data["header"]["cv_title"], styleH2)
    Story.append(p)

    Story.append(Spacer(1,0.2*cm))
    p = Paragraph(data["header"]["personal_data"],styleN)
    Story.append(p)
    f.addFromList(Story,doc)

    Story = []
    for section in data["left_side"]:
        Story = article(mode,Story,section,data["left_side"][section])

    f = Frame(2*cm,2*cm,(PAGE_WIDTH / 2.6), 21*cm) #, showBoundary=1)
    f.addFromList(Story,doc)

    Story = []
    for section in data["rigth_side"]:
        Story = article(mode,Story,section,data["rigth_side"][section])

    f = Frame((PAGE_WIDTH / 1.9) , 2*cm,(PAGE_WIDTH / 2.5), 21*cm) #, showBoundary=1)
    f.addFromList(Story,doc)
    doc.save()

def get_colors():
        return [
            rgb(204, 153, 255),
            rgb(232, 190, 147),
            rgb(255, 102, 153),
            rgb(255, 102, 102),
            rgb(255, 148, 77),
            rgb(255, 255, 26),
            rgb(0, 153, 51),
            rgb(153, 204, 255),
            rgb(77, 166, 255),
            rgb(204, 102, 0)
        ]
