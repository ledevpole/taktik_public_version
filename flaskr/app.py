import os
from flask import *
from flask_sqlalchemy import SQLAlchemy
from werkzeug.utils import secure_filename
from markupsafe import escape
from make_pdf import *
import json

UPLOAD_FOLDER_RESUMES = './downloads/resumes/'
UPLOAD_FOLDER_IMGS = './downloads/imgs/'
UPLOAD_FOLDER_JSONS = './downloads/jsons'
ALLOWED_EXTENSIONS = {'json','txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif'}

app = Flask(__name__)

# Set the secret key to some random bytes. Keep this really secret!
app.secret_key = b'_5#mqdsfqsdfqsfdsqaly2L"F4Q8z\n\xec]/'

app.config['UPLOAD_FOLDER_RESUMES'] = UPLOAD_FOLDER_RESUMES
app.config['UPLOAD_FOLDER_IMGS'] = UPLOAD_FOLDER_IMGS
app.config['UPLOAD_FOLDER_JSONS'] = UPLOAD_FOLDER_JSONS

app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///test.db'
db = SQLAlchemy(app)


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

class Mail(db.Model):
    id = db.Column(db.Integer,primary_key=True)
    target = db.Column(db.String(200), nullable=False)
    subject = db.Column(db.String(200), nullable=False)
    body = db.Column(db.String(200), nullable=False)
    attachement = db.Column(db.String(200), nullable=True)
    status = db.Column(db.String(200), nullable=False)

    def __repr__(self):
        return '<Mail %r>' % self.id

class Sug(db.Model):
    id = db.Column(db.Integer,primary_key=True)
    txt = db.Column(db.String(200), nullable=False)
    access_type = db.Column(db.String(200), nullable=False)

    def __repr__(self):
        return '<Sug %r>' % self.id

@app.route("/")
def landingpage():
    return render_template('landingpage.html')

@app.route('/prospection')
def prospection():
    return render_template('prospection.html')

@app.route("/mailing", methods=['POST', 'GET'])
def mailing():
    if request.method == 'POST':
        mail_target = request.form['target']
        mail_subject = request.form['subject']
        mail_body = request.form['body']
        if 'attachement' in request.files:
            file = request.files['attachement']
        if file.filename != '' and file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            file.save(os.path.join(app.config['UPLOAD_FOLDER_RESUMES'], filename))
        mail_status = "created"

        new_mail = Mail(
            target=mail_target,
            subject=mail_subject,
            body=mail_body,
            attachement=filename,
            status=mail_status
        )

        try:
            db.session.add(new_mail)
            db.session.commit()
            return redirect('/mailing')
        except:
            return 'There was an issue adding your mail'
    else:
        mails = Mail.query.all()
        return render_template('mailing.html',mails=mails)

@app.route('/mailing/delete/<int:id>')
def delete_mail(id):
    mail_to_delete = Mail.query.get_or_404(id)

    try:
        db.session.delete(mail_to_delete)
        db.session.commit()
        return redirect('/mailing')
    except:
        return 'There was a problem deleting that mail'

@app.route('/mailing/update/<int:id>', methods=['GET','POST'])
def update_mail(id):
    mail = Mail.query.get_or_404(id)
    if request.method == 'POST':
        mail.target = request.form['target']
        mail.subject = request.form['subject']
        mail.body = request.form['body']
        if 'attachement' in request.files:
            file = request.files['attachement']
        if file.filename != '' and file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            file.save(os.path.join(app.config['UPLOAD_FOLDER_RESUMES'], filename))
            mail.attachement = filename
        mail.status = request.form['status']

        try:
            db.session.commit()
            return redirect('/mailing')
        except:
            return 'There was a problem updating that mail'
    else:
        return render_template('mailing_update.html',mail=mail)


@app.route('/statistics')
def statistics():
    return redirect('/')

@app.route('/cvmaker', methods=['POST', 'GET'])
def cvmaker():
    if request.method == 'POST':
        #img
        if 'img' in request.files:
            img = request.files['img']

            if img.filename != '' and img and allowed_file(img.filename):
                img_name= secure_filename(img.filename)
                img.save(os.path.join(app.config['UPLOAD_FOLDER_IMGS'], img_name))
            else:
                flash('No image part!')
                return render_template('cvmaker.html')
        #json
        if 'json' in request.files:
            file = request.files['json']

        else:
            flash('No json part')
        if file.filename != '' and file and allowed_file(file.filename):
            json_name= secure_filename(file.filename)
            json_path = os.path.join(app.config['UPLOAD_FOLDER_JSONS'], json_name)
            file.save(json_path)
        else:
            flash('No json part!')
            return render_template('cvmaker.html')
            #interval
        mode = request.form['interval']
        rgbs = get_colors()

        with open(json_path) as f:
            data = json.load(f)

        for i in range(0,len(rgbs)):
           path = app.config['UPLOAD_FOLDER_RESUMES']
           img_path = os.path.join(app.config['UPLOAD_FOLDER_IMGS'], img_name)
           color = rgbs[i]
           create_basic_resume(data,img_path,f"{path}cv{i}.pdf",color,mode)
           create_basic_resume(data,img_path,f"{path}cv{i}_reverse.pdf",color,mode,reverse=True)

        return redirect('/')
    else:

        return render_template('cvmaker.html')

if __name__ == "__main__":
    app.run(debug=True)
